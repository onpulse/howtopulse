package main

import (
	"gitlab.com/onpulse/howtopulse/cmd"
)

func main() {
	cmd.Execute()
}
